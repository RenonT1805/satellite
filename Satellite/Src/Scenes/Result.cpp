﻿
Result::Result(const InitData& init) : IScene(init)
{
}

void Result::update()
{
	auto center = Scene::Center();
	center.x -= 80;
	if (SimpleGUI::Button(U"タイトルへ戻る", center))
	{
		changeScene(U"Title", 0.5);
	}
}

void Result::draw() const
{
	ClearPrint();

	Scene::SetBackground(ColorF(0.3, 0.4, 0.5));

	FontAsset(U"TitleFont")(U"Score : " + Format(getData().score)).drawAt(Scene::Center().x, Scene::Center().y - 200);
}
