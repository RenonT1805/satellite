﻿#pragma once

class Game : public App::Scene
{
public:

	Game(const InitData& init);

	void update() override;

	void draw() const override;

private:

	const Texture pien;
	World world;
	ComponentsManager& cm;
	Array<Entity> entities;
	Entity id;

	double spawnTime = 1.0;

	int hp = 100;

};
