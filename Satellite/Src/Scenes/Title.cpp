﻿
Title::Title(const InitData& init) : IScene(init)
{
	getData().score = 0;
}

void Title::update()
{
	auto center = Scene::Center();
	center.x -= 50;
	if (SimpleGUI::Button(U"スタート", center))
	{
		changeScene(U"Game");
	}
}

void Title::draw() const
{
	ClearPrint();

	Scene::SetBackground(ColorF(0.3, 0.4, 0.5));

	FontAsset(U"TitleFont")(U"ピエン地獄").drawAt(Scene::Center().x, Scene::Center().y - 200);
}
