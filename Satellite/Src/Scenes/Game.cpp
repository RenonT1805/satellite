﻿
Game::Game(const InitData& init) :
	IScene(init), pien(Emoji(U"😥")), cm(world.GetComponentsManager())
{
	world.AddSystem<Systems::LinerMove>();
	world.AddSystem<Systems::Hit>([this](World* world) {
		auto&& instance = std::make_shared<Systems::Hit>();
		instance->world = world;
		instance->hp = &hp;
		return instance;
	});
}
void Game::update()
{
	static double spawnTimeCount = 0;
	spawnTimeCount += Scene::DeltaTime();

	while (spawnTimeCount > spawnTime)
	{
		spawnTimeCount -= spawnTime;
		entities.push_back(id);
		cm.Add<Vec2>(id, Scene::Center());
		cm.Add<Components::CircleSize>(id, { id, 25 });
		cm.Add<Components::MoveVector>(id, { id, Random(0.0, 2 * 3.14), Random(1.0, 10.0) });
		id++;
	}

	entities.remove_if([this](const Entity& e) {
		if (-100 > cm.Get<Vec2>(e)->x || Scene::Size().x + 100 < cm.Get<Vec2>(e)->x ||
			-100 > cm.Get<Vec2>(e)->y || Scene::Size().y + 100 < cm.Get<Vec2>(e)->y)
		{
			cm.Remove<Vec2>(e);
			cm.Remove<Components::CircleSize>(e);
			cm.Remove<Components::MoveVector>(e);
			return true;
		}
		return false;
	});

	auto cursorPos = Cursor::Pos();
	if (0 > cursorPos.x || Scene::Size().x < cursorPos.x ||
		0 > cursorPos.y || Scene::Size().y < cursorPos.y)
	{
		hp--;
	}

	if (hp <= 0)
	{
		changeScene(U"Result", 0.1);
	}

	getData().score += Scene::DeltaTime() * 100.0;

	spawnTime = 1.0 / (getData().score / 100.0);

	world.Update(Scene::Time());
}

void Game::draw() const
{
	ClearPrint();

	Scene::SetBackground(ColorF(0.3, 0.4, 0.5));

	auto components = cm.GetArray<Vec2>();
	if (components != nullptr)
	{
		for (auto& component : *components)
		{
			pien.scaled(0.5).drawAt(component);
		}
	}

	Print << U"Score : " << getData().score;
	Print << U"HP : " << hp;
}
