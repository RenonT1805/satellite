﻿#pragma once

namespace Systems
{
	struct Hit : public SystemBase
	{
	public:

		void Update(double dt) override
		{
			auto components = world->GetComponentsManager().GetArray<Components::CircleSize>();
			if (components != nullptr)
			{
				for (auto& component : *components)
				{
					auto pos = world->GetComponentsManager().Get<Vec2>(component.entity);
					auto cursor = Cursor::Pos();
					
					if (sqrt(pow(cursor.x - pos->x, 2) + pow(cursor.y - pos->y, 2)) < component.size)
					{
						*hp -= 1;
					}
				}
			}
		}

		int* hp;
	};
}

