﻿#pragma once

namespace Systems
{
	struct LinerMove : public SystemBase
	{
	public:

		void Update(double dt) override
		{
			auto components = world->GetComponentsManager().GetArray<Components::MoveVector>();
			if (components != nullptr)
			{
				for (auto& component : *components)
				{
					auto pos = world->GetComponentsManager().Get<Vec2>(component.entity);
					pos->x += cos(component.angle) * component.power;
					pos->y -= sin(component.angle) * component.power;
				}
			}
		}

	};
}

