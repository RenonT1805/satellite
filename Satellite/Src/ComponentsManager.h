﻿#pragma once

template<class T>
class Chunk
{
public:

	T* Get(const Entity& entity)
	{
		for (size_t i = 0; i < entities.size(); i++)
		{
			if (entities[i] == entity)
			{
				return &components[i];
			}
		}
		return nullptr;
	}

	T* Add(const Entity& entity, T v)
	{
		components.push_back(v);
		entities.push_back(entity);
		return &components.back();
	}

	void Remove(const Entity& entity)
	{
		for (size_t i = 0; i < entities.size(); i++)
		{
			if (entities[i] == entity)
			{
				components.erase(components.begin() + i);
				entities.erase(entities.begin() + i);
				break;
			}
		}
	}

	Array<T>* GetArray()
	{
		return &components;
	}

	const std::optional<Entity> GetEntity(const T* component)
	{
		for (size_t i = 0; i < components.size(); i++)
		{
			if (component == &components[i])
			{
				return entities[i];
			}
		}
		return std::nullopt;
	}

private:

	Array<T> components;
	Array<Entity> entities;
};

template <class T>
concept Component = requires (Chunk<T> x, Entity e) {
	{ x.Get(e) } -> std::convertible_to<T*>;
	{ x.GetArray() } -> std::convertible_to<Array<T>*>;
	x.Add(e, T());
	x.Remove(e);
};

class ComponentsManager
{
public:

	template<Component T>
	T* Get(const Entity& entity)
	{
		if (!chunks.contains(typeid(T)))
		{
			return nullptr;
		}
		return ((Chunk<T>*)(chunks[typeid(T)].get()))->Get(entity);
	}

	template<Component T>
	T* Add(const Entity& entity, T v)
	{
		auto&& chunk = (Chunk<T>*)(chunks[typeid(T)].get());
		if (!chunk)
		{
			chunks[typeid(T)] = ChunkPtr((void*)new Chunk<T>());
			chunk = (Chunk<T>*)(chunks[typeid(T)].get());;
		}
		return chunk->Add(entity, v);
	}

	template<Component T>
	void Remove(const Entity& entity)
	{
		if (!chunks.contains(typeid(T)))
		{
			return;
		}
		((Chunk<T>*)(chunks[typeid(T)].get()))->Remove(entity);
	}

	template<Component T>
	Array<T>* GetArray()
	{
		if (!chunks.contains(typeid(T)))
		{
			return nullptr;
		}
		return ((Chunk<T>*)(chunks[typeid(T)].get()))->GetArray();
	}

	template<Component T>
	const std::optional<Entity> GetEntity(const T* component)
	{
		if (!chunks.contains(typeid(T)))
		{
			return std::nullopt;
		}
		return ((Chunk<T>*)(chunks[typeid(T)].get()))->GetEntity(component);
	}

private:

	using ChunkPtr = std::shared_ptr<void>;

	HashTable<std::type_index, ChunkPtr> chunks;
};
