﻿#pragma once

class World;

class SystemBase
{
public:

	virtual void Update(double dt) = 0;

	virtual ~SystemBase() = default;

	World* world = nullptr;
	int priority = 0;

};
