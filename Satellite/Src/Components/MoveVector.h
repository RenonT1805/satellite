﻿#pragma once

namespace Components
{
	struct MoveVector
	{
		MoveVector() = default;

		MoveVector(Entity entity, double angle, double power) :
			entity(entity), angle(angle), power(power)
		{
		}

		Entity entity;
		double angle, power;
	};
}

