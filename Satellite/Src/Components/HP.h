﻿#pragma once

namespace Components
{
	struct HP
	{
		HP() = default;

		HP(int hp) :
			hp(hp)
		{
		}

		int hp;
	};
}

