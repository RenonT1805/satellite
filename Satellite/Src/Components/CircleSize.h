﻿#pragma once

namespace Components
{
	struct CircleSize
	{
		CircleSize() = default;

		CircleSize(Entity entity, double size) :
			entity(entity), size(size)
		{
		}

		Entity entity;
		double size;
	};
}

