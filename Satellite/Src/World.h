﻿#pragma once

class World
{
public:

	template<class T>
	const SystemBase* AddSystem(
		const std::function<std::shared_ptr<T>(World*)>& initializer = [](World* world) {
			auto&& instance = std::make_shared<T>();
			instance->world = world;
			return instance;
	})
	{
		auto&& instance = initializer(this);
		systems.push_back(instance);
		systems.sort_by([](const std::shared_ptr<SystemBase>& x, const std::shared_ptr<SystemBase>& y) { return x->priority > y->priority; });
		return instance.get();
	}

	void RemoveSystem(const SystemBase* system)
	{
		systems.remove_if([system](const std::shared_ptr<SystemBase>& v) {
			return v.get() == system;
		});
	}

	void Update(double dt)
	{
		for (auto& system : systems)
		{
			system->Update(dt);
		}
	}

	ComponentsManager& GetComponentsManager()
	{
		return componentsManager;
	}

private:

	Array<std::shared_ptr<SystemBase>> systems;
	ComponentsManager componentsManager;
};
