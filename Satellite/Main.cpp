﻿#include "stdafx.h"

void Main()
{
	FontAsset::Register(U"TitleFont", 60, Typeface::Heavy);

	Window::Resize(Size{1000, 800});

	App manager;
	manager.add<Title>(U"Title");
	manager.add<Game>(U"Game");
	manager.add<Result>(U"Result");

	while (System::Update())
	{
		if (!manager.update())
		{
			break;
		}
	}
}
